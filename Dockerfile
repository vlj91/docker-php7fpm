FROM vlj91/base:latest

MAINTAINER Vaughan Jones <vaughan.jones@nbcuni.com>
LABEL image.type="php7-fpm"

EXPOSE 9000

RUN echo "@testing http://dl-4.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
    apk update && \
    apk add \
        php7 \
        php7-fpm

ADD php-fpm.conf /etc/php7/php-fpm.conf
ADD php.ini /etc/php7/conf.d/00-setting.ini
ADD www.conf /etc/php7/php-fpm.d/www.conf

CMD /usr/sbin/php-fpm7 -R --nodaemonize
